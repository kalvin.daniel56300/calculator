# Contributors

Melanie Levesque - Killian Levaillant - Kalvin Daniel  

# Calculator

This project is an online calculator that can do simple operations:

- Sum
- Subtraction
- Multiplication
- Division

You can type numbers directly onto your keyboard instead of using the calculator buttons.

You can also see and your calculation history and extract it by clicking the "export to csv" button.

# Getting Started

## Run Locally

In the project directory, you can run:

Install dependencies

```bash
  npm install
```

Then:

```bash
  npm start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

# Documentation

### Buttons

| Button | Description                       |
| :----- | :-------------------------------- |
| `C`    | Clear the calcul                  |
| `CE`   | Clear the number you are writting |
| `=`    | Show the result                   |

## How to use of the keyboard keys ?

You can use your numeric keypad.

| Button                                | Description                             |
| :-----------------------------------  | :-------------------------------------  |
| `> right arrow` or `backspace` or `c` | =`C`, Clear the calcul calcul           |
| `< left arrow` or `x`                 | =`CE` Clear the number you are writting |
| `=`                                   | Show the result                         |
| `a`                                   | addition                                |
| `s`                                   | subtraction                             |
| `m`                                   | multiplication                          |
| `d`                                   | division                                |
| `;` or `.`                            | the dot                                 |
