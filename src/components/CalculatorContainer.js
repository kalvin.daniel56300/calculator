import "../styles/CalculatorContainer.css";

const CalculatorContainer = ({ children }) => {
  return <div className="CalculatorContainer d-flex-column">{children}</div>;
};

export default CalculatorContainer;