import "../styles/CalculatorScreen.css";

const CalculatorScreen = ({ currentresult, value }) => {
  return (
    <>
    <h1 className="CalculatorScreen" mode="single" max={70}>
      {value}
    </h1>
    <h2 className="CalculatorCurrentResult">Résultat : {currentresult}</h2>
    </>
  );
};

export default CalculatorScreen;