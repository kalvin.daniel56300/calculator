import React from 'react'
import "../styles/Historical.css";


function downloadCSV(csv, filename) {
  var csvFile;
  var downloadLink;

  // CSV file
  csvFile = new Blob([csv], {type: "text/csv"});

  // Download link
  downloadLink = document.createElement("a");

  // File name
  downloadLink.download = filename;

  // Create a link to the file
  downloadLink.href = window.URL.createObjectURL(csvFile);

  // Hide download link
  downloadLink.style.display = "none";

  // Add the link to DOM
  document.body.appendChild(downloadLink);

  // Click download link
  downloadLink.click();
}

function exportTableToCSV(history) {
  console.log(history)
  var csv = [];
  var filename = "exportCalculator.csv"
  
  var header = "date,operation,result"
  csv.push(header)
  for(var i = 0; i < history.length; i++){
    console.log(history[i])
    var row = history[i].date + ',' + history[i].operation + ',' + history[i].result
    csv.push(row)
  }

  // Download CSV file
  downloadCSV(csv.join("\n"), filename);
}

const Historical = ({array}) => {
  return (
    <div className='HistoricalContainer p-24 border-radius-8'>
      <h2 className='text-center'>Historique</h2>
      <button className='mb-8' onClick={() => exportTableToCSV(array)}>Exporter en CSV</button>
      {array.map(history => (
        <div className="d-flex" key={history.date.toLocaleTimeString()}>
          ({history.date.toLocaleTimeString()}) {history.operation} = {history.result}
        </div>
      ))}
    </div>
  )
}

export default Historical