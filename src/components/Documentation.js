import React from 'react'
import "../styles/Documentation.css";
import Button from './Button';

function Documentation() {
    return(
        <div className='DocumentationBody'>
            <div className='d-flex-end'>
                <a href="#calculator"><Button className={''} value={'Go Back to Calculator ▲'}></Button></a>
            </div>
            <h1 className='text-center'>Calculator instructions</h1>
            
            This project is an online calculator that can do simple operations:
            <ul>
                <li>Sum</li>
                <li>Substraction</li>
                <li>Multiplication</li>
                <li>Division</li>
            </ul>
            You can type numbers directly onto your keyboard instead of using the calculator buttons.
            You can also see and your calculation history and extract it by clicking the "export to csv" button.
            
            <h2 className='text-center'>How to use the calculator ?</h2>
            
                Here are the buttons actions:
                <table>
                    <thead>
                        <tr>
                            <th>Button</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>C</td>
                            <td>Clear the calcul</td>
                        </tr>
                        <tr>
                            <td>CE</td>
                            <td>Clear the number you are writting</td>
                        </tr>
                        <tr>
                            <td>=</td>
                            <td>Show the result</td>
                        </tr>
                    </tbody>
                </table>
            
            <h2 className='text-center'>How to use the keyboard ?</h2>
                You can also use the numeric keypad.
                Here are the shortcut you can use to interact with the online calculator:
                
                    <table>
                        <thead>
                        <tr>
                            <th>Button</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> &gt; right arrow or backspace or c</td>
                                <td>Clear the calcul</td>
                            </tr>
                            <tr>
                                <td> &lt; left arrow or x</td>
                                <td>Clear the number you are writting</td>
                            </tr>
                            <tr>
                                <td>Enter or =</td>
                                <td>Show the result</td>
                            </tr>
                            <tr>
                                <td>a or +</td>
                                <td>Addition</td>
                            </tr>
                            <tr>
                                <td>s or -</td>
                                <td>Subtraction</td>
                            </tr>
                            <tr>
                                <td>m or *</td>
                                <td>Multiplication</td>
                            </tr>
                            <tr>
                                <td>d or /</td>
                                <td>Division</td>
                            </tr>
                            <tr>
                                <td>; or .</td>
                                <td>The dot</td>
                            </tr>
                        </tbody>
                    </table>
                
            <h2 className='text-center'>Results history</h2>
                <p>
                    The result history is placed at the right of the calculator.
                    You can download your calculations history to a CSV file.
                </p>
            <h3 className='text-center'>What is a CSV file ?</h3>
            <p>
                A CSV file (Comma-separated values) is a text format that can be used in a spreadsheet (like Excel for example).
                Each row is a row in a spreadsheet. They are separated by sparators (comma) that corresponds to the columns of the spreadsheet.
            </p>
        </div>
    )
}

export default Documentation;