import './App.css';
import CalculatorContainer from './components/CalculatorContainer';
import CalculatorScreen from './components/CalculatorScreen';
import ButtonBox from './components/ButtonBox';
import Button from './components/Button';
import { useState, useEffect } from 'react';
import Historical from './components/Historical';
import Documentation from './components/Documentation';
import HelpButton from './components/HelpButton';

const toLocaleString = (num) =>
  String(num).replace(/(?<!\..*)(\d)(?=(?:\d{3})+(?:\.|$))/g, "$1 ");

const removeSpaces = (num) => num.toString().replace(/\s/g, "");

const btnValues = [
  ["C", "CE", "+-", "/"],
  [7, 8, 9, "X"],
  [4, 5, 6, "-"],
  [1, 2, 3, "+"],
  [0, ".", "="],
];

function App() {

  useEffect(() => {
    window.addEventListener('keydown', handleKeyDown);
    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  });

  const [data, setData] = useState({
    historical: [],
    operator: "",
    number: 0,
    result: 0,
  })

  const resetAll = () => {
    setData({
      ...data,
      operator: "",
      number: 0,
      result: 0,
    });
  }

  const resetCurrent = () => {
    setData({
      ...data,
      operator: "",
      number: 0,
    });
  }

  const calculate = () => {
    if (data.operator && data.number) {
      const math = (a, b, operator) =>
        operator === "+"
          ? a + b
          : operator === "-"
          ? a - b
          : operator === "X"
          ? a * b
          : a / b;

      let _result = data.number === "0" && data.operator === "/"
        ? "Impossible de diviser par 0"
        : math(Number(removeSpaces(data.result)), Number(removeSpaces(data.number)), data.operator)

      let _historical = [...data.historical]
      _historical.push({
        date: new Date(),
        operation: `${Number(removeSpaces(data.result))} ${data.operator} ${Number(removeSpaces(data.number))}`,
        result: _result,
      })
  
      setData({
        ...data,
        historical: _historical,
        result: _result,
        operator: "",
        number: 0,
      });
    }
  }

  const operator = (e) => {
    e.preventDefault();
    const value = e.target.innerHTML;

    setData({
      ...data,
      operator: value,
      result: !data.result && data.number ? data.number : data.result,
      number: 0,
    });
  }

  const operatorPressed = (e, op) => {
    e.preventDefault();
    const value = op;

    setData({
      ...data,
      operator: value,
      result: !data.result && data.number ? data.number : data.result,
      number: 0,
    });
  }

  const comma = (e) => { 
    e.preventDefault();
    const value = e.target.innerHTML;

    setData({
      ...data,
      number: !data.number.toString().includes(".") ? data.number + value : data.number,
    });
  }

  const commaPressed = (e, nb) => { 
    e.preventDefault();
    const value = nb;

    setData({
      ...data,
      number: !data.number.toString().includes(".") ? data.number + value : data.number,
    });
  }

  const numberClick = (e) => { 
    e.preventDefault();
    const value = e.target.innerHTML;
  
    setData({
      ...data,
      number:
        data.number === 0 && value === "0"
          ? "0"
          : removeSpaces(data.number) % 1 === 0
          ? toLocaleString(Number(data.number + value))
          : toLocaleString(data.number + value),
      result: !data.operator ? 0 : data.result,
    });
   
  }

  const numberPressed = (e, nb) =>{
    e.preventDefault();
    const value = nb;

    setData({
      ...data,
      number:
        data.number === 0 && value === "0"
          ? "0"
          : removeSpaces(data.number) % 1 === 0
          ? toLocaleString(Number(data.number + value))
          : toLocaleString(data.number + value),
      result: !data.operator ? 0 : data.result,
    });
  }

  const invertSign = () => {
    setData({
      ...data,
      number: data.number ? data.number * -1 : 0,
      result: data.result ? data.result * -1 : 0,
      operator: "",
    });
  }

  const handleClick = (e, type) => {
    switch (type) {
      case "C":
        resetAll()
        break;
      case "CE":
        resetCurrent()
        break;
      case "=":
        calculate()
        break;
      case "+-":
        invertSign()
        break;
      case "/":
      case "X":
      case "-":
      case "+":
        operator(e)
        break;
      case ".":
        comma(e)
        break;
      default:
        numberClick(e)
        break;
    }
  }

function handleKeyDown(e) {
  var key = e.code;
  if(key.includes("Digit") || key.includes("Numpad")){
    var nb = key.charAt(key.length - 1);
    if(!isNaN(nb)){
      numberPressed(e, nb);
    }
  }
  switch(key){
    // "C"
    case "KeyC":
    case "Backspace":
    case "ArrowRight": 
      resetAll()
      break;
    // "CE"
    case "KeyX": 
    case "ArrowLeft":
      resetCurrent()
      break;
    // "="
    case "Enter":
    case "NumpadEnter":
      calculate()
      break;
    // "+-"
    case "Equal":
      invertSign()
      break;
    // "/"
    case "Period":
    case "NumpadDivide":
      operatorPressed(e, "/");
      break;
    // "X"
    case "Semicolon":
    case "NumpadMultiply":
      operatorPressed(e, "X");
      break;
    // "-"
    case "KeyS":
    case "NumpadSubtract":
      operatorPressed(e, "-");
      break;
    // "+"
    case "KeyQ":
    case "NumpadAdd":
      operatorPressed(e, "+");
      break;
    // "."
    case "NumpadDecimal":
    case "Comma":
      commaPressed(e, ".");
      break;
    default:
      break;
  }
}

  return (
    <div >
      <div className="App" id="calculator">
        <div className='d-flex g-24'>
          <CalculatorContainer>
            <CalculatorScreen value={data.number } currentresult={data.result} />
            <ButtonBox>
            {
              btnValues.flat().map((btn, i) => {
                return (
                  <Button
                    key={i}
                    className={btn === "=" ? "equals" : (btn === "C" ? "action-c" : (btn === "CE" ? "action-ce" : ""))}
                    value={btn}
                    onClick={(e) => handleClick(e, btn)}
                  />
                );
              })
            }
            </ButtonBox>
          </CalculatorContainer>
          <Historical array={data.historical} />
        </div>
        <div className='d-flex-end'>
            <HelpButton/>
        </div>
      </div>
      <div id="doc">
        <Documentation/>
      </div>
    </div>
  );
}

export default App;
